/* global gapi */
import React, { Component } from 'react';
import { CLIENT_ID, API_KEY, DISCOVERY_DOCS, SCOPES } from '../configs';
import Calendar from '../components/Calendar';
import '../App.css';

class App extends Component {    
    constructor(props) {
        super(props);
        this.state = {
            events: [],
            userStatus: false
        };

        this.handleAuthClick = this.handleAuthClick.bind(this);
        this.initClient = this.initClient.bind(this);
        this.getAuthStatus = this.getAuthStatus.bind(this);
    }

    componentDidMount() {
        gapi.load('client:auth2', this.initClient);
    }
   
    //handle authentication form
    handleAuthClick(){
        gapi.auth2.getAuthInstance().signIn();
    }

    //get first and last day
    getFirstAndLastDay(){
        var current = new Date();
        var weekstart = current.getDate() - current.getDay();    
        var weekend = weekstart + 6;
        var sunday = new Date(current.setDate(weekstart));  
        var saturday = new Date(current.setDate(weekend));
        return {sunday: sunday, saturday: saturday};
    }
    
    //get authentication
    getAuthStatus(isSignedIn){
        let that = this;
        this.setState({
            userStatus: isSignedIn

        }, () => {
            if(this.state.userStatus){
                let weekDays = that.getFirstAndLastDay();

                gapi.client.calendar.events.list({
                    'calendarId': 'primary',
                    'timeMin': (weekDays.sunday).toISOString(),   
                    'timeMax': (weekDays.saturday).toISOString(),                   
                    'singleEvents': true,
                    'orderBy': 'startTime'

                  }).then(function(data){
                    let calEvents = data.result.items.map(function(resp, i){
                        return {
                            id: resp.id,
                            location: resp.location,
                            start: resp.start.dateTime,
                            end: resp.end.dateTime,
                            title: resp.summary,
                        };
                    })

                    return calEvents;
                }).then(function(resp){
                    that.setState({events: resp});

                }, function(reason) {
                    console.log('Error: ' + reason.result.error.message);
                })
            }    
        })
    }

    //initialise google api client
    initClient() {
        let that = this;        
        gapi.client.init({
            apiKey: API_KEY,
            discoveryDocs: DISCOVERY_DOCS,
            clientId: CLIENT_ID,
            scope: SCOPES
        
        }).then(function(){
            gapi.auth2.getAuthInstance().isSignedIn.listen(that.getAuthStatus);

            that.getAuthStatus(gapi.auth2.getAuthInstance().isSignedIn.get());

        }, function(reason) {
            console.log('Error: ' + reason.result.error.message);
        });
    }

    render() {                  
        return (
            <div className="App">
                
                {
                    this.state.userStatus ?
                        <Calendar events={this.state.events} />
                    :    
                        <header className="App-header">
                            <input type="button" name="auth" value="Sync Google calendar" onClick={this.handleAuthClick} />    
                        </header>   
                }
                                   
            </div>
        )
    }
}

export default App;
