import React from 'react'
import FullCalendar from 'fullcalendar-reactwrapper';
import 'fullcalendar/dist/fullcalendar.print.min.css';
import 'fullcalendar/dist/fullcalendar.min.css';

const Calendar = (props) => {
    return (
        <div id="example-component">
            <FullCalendar
                id="abc"
                header={{
                    left: '',
                    center: 'title',
                    right: ''
                }}
                defaultView="listWeek"                
                events={props.events}
            />
        </div>
    );
}

export default Calendar;
